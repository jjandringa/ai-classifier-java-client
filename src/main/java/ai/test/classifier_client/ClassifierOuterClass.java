//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package ai.test.classifier_client;

import com.google.protobuf.AbstractParser;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.ExtensionRegistry;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MapEntry;
import com.google.protobuf.MapField;
import com.google.protobuf.Message;
import com.google.protobuf.MessageOrBuilder;
import com.google.protobuf.Parser;
import com.google.protobuf.UnknownFieldSet;
import com.google.protobuf.Descriptors.Descriptor;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.Descriptors.FileDescriptor;
import com.google.protobuf.Descriptors.OneofDescriptor;
//import com.google.protobuf.GeneratedMessageV3.BuilderParent;
import com.google.protobuf.GeneratedMessageV3.FieldAccessorTable;
//import com.google.protobuf.GeneratedMessageV3.UnusedPrivateParameter;
import com.google.protobuf.WireFormat.FieldType;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public final class ClassifierOuterClass {
    private static final Descriptor internal_static_ElementClassificationRequest_descriptor;
    private static final FieldAccessorTable internal_static_ElementClassificationRequest_fieldAccessorTable;
    private static final Descriptor internal_static_ElementClassificationRequest_ElementImagesEntry_descriptor;
    private static final FieldAccessorTable internal_static_ElementClassificationRequest_ElementImagesEntry_fieldAccessorTable;
    private static final Descriptor internal_static_ElementClassificationResult_descriptor;
    private static final FieldAccessorTable internal_static_ElementClassificationResult_fieldAccessorTable;
    private static final Descriptor internal_static_ClassifiedElements_descriptor;
    private static final FieldAccessorTable internal_static_ClassifiedElements_fieldAccessorTable;
    private static final Descriptor internal_static_ClassifiedElements_ClassificationsEntry_descriptor;
    private static final FieldAccessorTable internal_static_ClassifiedElements_ClassificationsEntry_fieldAccessorTable;
    private static FileDescriptor descriptor;

    private ClassifierOuterClass() {
    }

    public static void registerAllExtensions(ExtensionRegistryLite registry) {
    }

    public static void registerAllExtensions(ExtensionRegistry registry) {
        registerAllExtensions((ExtensionRegistryLite)registry);
    }

    public static FileDescriptor getDescriptor() {
        return descriptor;
    }

    static {
        String[] descriptorData = new String[]{"\n\u0010classifier.proto\"é\u0001\n\u001cElementClassificationRequest\u0012\u0011\n\tlabelHint\u0018\u0001 \u0001(\t\u0012G\n\relementImages\u0018\u0002 \u0003(\u000b20.ElementClassificationRequest.ElementImagesEntry\u0012\u001b\n\u0013confidenceThreshold\u0018\u0003 \u0001(\u0001\u0012\u001a\n\u0012allowWeakerMatches\u0018\u0004 \u0001(\b\u001a4\n\u0012ElementImagesEntry\u0012\u000b\n\u0003key\u0018\u0001 \u0001(\t\u0012\r\n\u0005value\u0018\u0002 \u0001(\f:\u00028\u0001\"[\n\u001bElementClassificationResult\u0012\r\n\u0005label\u0018\u0001 \u0001(\t\u0012\u0012\n\nconfidence\u0018\u0002 \u0001(\u0001\u0012\u0019\n\u0011confidenceForHint\u0018\u0003 \u0001(\u0001\"\u00ad\u0001\n\u0012ClassifiedElements\u0012A\n\u000fclassifications\u0018\u0001 \u0003(\u000b2(.ClassifiedElements.ClassificationsEntry\u001aT\n\u0014ClassificationsEntry\u0012\u000b\n\u0003key\u0018\u0001 \u0001(\t\u0012+\n\u0005value\u0018\u0002 \u0001(\u000b2\u001c.ElementClassificationResult:\u00028\u00012T\n\nClassifier\u0012F\n\u0010ClassifyElements\u0012\u001d.ElementClassificationRequest\u001a\u0013.ClassifiedElementsB\u001b\n\u0019ai.test.classifier_clientb\u0006proto3"};
        descriptor = FileDescriptor.internalBuildGeneratedFileFrom(descriptorData, new FileDescriptor[0]);
        internal_static_ElementClassificationRequest_descriptor = (Descriptor)getDescriptor().getMessageTypes().get(0);
        internal_static_ElementClassificationRequest_fieldAccessorTable = new FieldAccessorTable(internal_static_ElementClassificationRequest_descriptor, new String[]{"LabelHint", "ElementImages", "ConfidenceThreshold", "AllowWeakerMatches"});
        internal_static_ElementClassificationRequest_ElementImagesEntry_descriptor = (Descriptor)internal_static_ElementClassificationRequest_descriptor.getNestedTypes().get(0);
        internal_static_ElementClassificationRequest_ElementImagesEntry_fieldAccessorTable = new FieldAccessorTable(internal_static_ElementClassificationRequest_ElementImagesEntry_descriptor, new String[]{"Key", "Value"});
        internal_static_ElementClassificationResult_descriptor = (Descriptor)getDescriptor().getMessageTypes().get(1);
        internal_static_ElementClassificationResult_fieldAccessorTable = new FieldAccessorTable(internal_static_ElementClassificationResult_descriptor, new String[]{"Label", "Confidence", "ConfidenceForHint"});
        internal_static_ClassifiedElements_descriptor = (Descriptor)getDescriptor().getMessageTypes().get(2);
        internal_static_ClassifiedElements_fieldAccessorTable = new FieldAccessorTable(internal_static_ClassifiedElements_descriptor, new String[]{"Classifications"});
        internal_static_ClassifiedElements_ClassificationsEntry_descriptor = (Descriptor)internal_static_ClassifiedElements_descriptor.getNestedTypes().get(0);
        internal_static_ClassifiedElements_ClassificationsEntry_fieldAccessorTable = new FieldAccessorTable(internal_static_ClassifiedElements_ClassificationsEntry_descriptor, new String[]{"Key", "Value"});
    }

    public static final class ClassifiedElements extends GeneratedMessageV3 implements ClassifierOuterClass.ClassifiedElementsOrBuilder {
        private static final long serialVersionUID = 0L;
        public static final int CLASSIFICATIONS_FIELD_NUMBER = 1;
        private MapField<String, ClassifierOuterClass.ElementClassificationResult> classifications_;
        private byte memoizedIsInitialized;
        private static final ClassifierOuterClass.ClassifiedElements DEFAULT_INSTANCE = new ClassifierOuterClass.ClassifiedElements();
        private static final Parser<ClassifierOuterClass.ClassifiedElements> PARSER = new AbstractParser<ClassifierOuterClass.ClassifiedElements>() {
            public ClassifierOuterClass.ClassifiedElements parsePartialFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
                return new ClassifierOuterClass.ClassifiedElements(input, extensionRegistry);
            }
        };

        private ClassifiedElements(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
            super(builder);
            this.memoizedIsInitialized = -1;
        }

        private ClassifiedElements() {
            this.memoizedIsInitialized = -1;
        }

        protected Object newInstance(UnusedPrivateParameter unused) {
            return new ClassifierOuterClass.ClassifiedElements();
        }

        public final UnknownFieldSet getUnknownFields() {
            return this.unknownFields;
        }

        private ClassifiedElements(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            this();
            if (extensionRegistry == null) {
                throw new NullPointerException();
            } else {
                boolean mutable_bitField0_ = false;
                com.google.protobuf.UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder();

                try {
                    boolean done = false;

                    while(!done) {
                        int tag = input.readTag();
                        switch(tag) {
                            case 0:
                                done = true;
                                break;
                            case 10:
                                if (!(mutable_bitField0_ & true)) {
                                    this.classifications_ = MapField.newMapField(ClassifierOuterClass.ClassifiedElements.ClassificationsDefaultEntryHolder.defaultEntry);
                                    mutable_bitField0_ |= true;
                                }

                                MapEntry<String, ClassifierOuterClass.ElementClassificationResult> classifications__ = (MapEntry)input.readMessage(ClassifierOuterClass.ClassifiedElements.ClassificationsDefaultEntryHolder.defaultEntry.getParserForType(), extensionRegistry);
                                this.classifications_.getMutableMap().put(classifications__.getKey(), classifications__.getValue());
                                break;
                            default:
                                if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                    done = true;
                                }
                        }
                    }
                } catch (InvalidProtocolBufferException var12) {
                    throw var12.setUnfinishedMessage(this);
                } catch (IOException var13) {
                    throw (new InvalidProtocolBufferException(var13)).setUnfinishedMessage(this);
                } finally {
                    this.unknownFields = unknownFields.build();
                    this.makeExtensionsImmutable();
                }

            }
        }

        public static final Descriptor getDescriptor() {
            return ClassifierOuterClass.internal_static_ClassifiedElements_descriptor;
        }

        protected MapField internalGetMapField(int number) {
            switch(number) {
                case 1:
                    return this.internalGetClassifications();
                default:
                    throw new RuntimeException("Invalid map field number: " + number);
            }
        }

        protected FieldAccessorTable internalGetFieldAccessorTable() {
            return ClassifierOuterClass.internal_static_ClassifiedElements_fieldAccessorTable.ensureFieldAccessorsInitialized(ClassifierOuterClass.ClassifiedElements.class, ClassifierOuterClass.ClassifiedElements.Builder.class);
        }

        private MapField<String, ClassifierOuterClass.ElementClassificationResult> internalGetClassifications() {
            return this.classifications_ == null ? MapField.emptyMapField(ClassifierOuterClass.ClassifiedElements.ClassificationsDefaultEntryHolder.defaultEntry) : this.classifications_;
        }

        public int getClassificationsCount() {
            return this.internalGetClassifications().getMap().size();
        }

        public boolean containsClassifications(String key) {
            if (key == null) {
                throw new NullPointerException();
            } else {
                return this.internalGetClassifications().getMap().containsKey(key);
            }
        }

        /** @deprecated */
        @Deprecated
        public Map<String, ClassifierOuterClass.ElementClassificationResult> getClassifications() {
            return this.getClassificationsMap();
        }

        public Map<String, ClassifierOuterClass.ElementClassificationResult> getClassificationsMap() {
            return this.internalGetClassifications().getMap();
        }

        public ClassifierOuterClass.ElementClassificationResult getClassificationsOrDefault(String key, ClassifierOuterClass.ElementClassificationResult defaultValue) {
            if (key == null) {
                throw new NullPointerException();
            } else {
                Map<String, ClassifierOuterClass.ElementClassificationResult> map = this.internalGetClassifications().getMap();
                return map.containsKey(key) ? (ClassifierOuterClass.ElementClassificationResult)map.get(key) : defaultValue;
            }
        }

        public ClassifierOuterClass.ElementClassificationResult getClassificationsOrThrow(String key) {
            if (key == null) {
                throw new NullPointerException();
            } else {
                Map<String, ClassifierOuterClass.ElementClassificationResult> map = this.internalGetClassifications().getMap();
                if (!map.containsKey(key)) {
                    throw new IllegalArgumentException();
                } else {
                    return (ClassifierOuterClass.ElementClassificationResult)map.get(key);
                }
            }
        }

        public final boolean isInitialized() {
            byte isInitialized = this.memoizedIsInitialized;
            if (isInitialized == 1) {
                return true;
            } else if (isInitialized == 0) {
                return false;
            } else {
                this.memoizedIsInitialized = 1;
                return true;
            }
        }

        public void writeTo(CodedOutputStream output) throws IOException {
            GeneratedMessageV3.serializeStringMapTo(output, this.internalGetClassifications(), ClassifierOuterClass.ClassifiedElements.ClassificationsDefaultEntryHolder.defaultEntry, 1);
            this.unknownFields.writeTo(output);
        }

        public int getSerializedSize() {
            int size = this.memoizedSize;
            if (size != -1) {
                return size;
            } else {
                size = 0;

                MapEntry classifications__;
                for(Iterator var2 = this.internalGetClassifications().getMap().entrySet().iterator(); var2.hasNext(); size += CodedOutputStream.computeMessageSize(1, classifications__)) {
                    Entry<String, ClassifierOuterClass.ElementClassificationResult> entry = (Entry)var2.next();
                    classifications__ = ClassifierOuterClass.ClassifiedElements.ClassificationsDefaultEntryHolder.defaultEntry.newBuilderForType().setKey(entry.getKey()).setValue(entry.getValue()).build();
                }

                size += this.unknownFields.getSerializedSize();
                this.memoizedSize = size;
                return size;
            }
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            } else if (!(obj instanceof ClassifierOuterClass.ClassifiedElements)) {
                return super.equals(obj);
            } else {
                ClassifierOuterClass.ClassifiedElements other = (ClassifierOuterClass.ClassifiedElements)obj;
                if (!this.internalGetClassifications().equals(other.internalGetClassifications())) {
                    return false;
                } else {
                    return this.unknownFields.equals(other.unknownFields);
                }
            }
        }

        public int hashCode() {
            if (this.memoizedHashCode != 0) {
                return this.memoizedHashCode;
            } else {
                int hash = 41;
                hash = 19 * hash + getDescriptor().hashCode();
                if (!this.internalGetClassifications().getMap().isEmpty()) {
                    hash = 37 * hash + 1;
                    hash = 53 * hash + this.internalGetClassifications().hashCode();
                }

                hash = 29 * hash + this.unknownFields.hashCode();
                this.memoizedHashCode = hash;
                return hash;
            }
        }

        public static ClassifierOuterClass.ClassifiedElements parseFrom(ByteBuffer data) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ClassifiedElements)PARSER.parseFrom(data);
        }

        public static ClassifierOuterClass.ClassifiedElements parseFrom(ByteBuffer data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ClassifiedElements)PARSER.parseFrom(data, extensionRegistry);
        }

        public static ClassifierOuterClass.ClassifiedElements parseFrom(ByteString data) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ClassifiedElements)PARSER.parseFrom(data);
        }

        public static ClassifierOuterClass.ClassifiedElements parseFrom(ByteString data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ClassifiedElements)PARSER.parseFrom(data, extensionRegistry);
        }

        public static ClassifierOuterClass.ClassifiedElements parseFrom(byte[] data) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ClassifiedElements)PARSER.parseFrom(data);
        }

        public static ClassifierOuterClass.ClassifiedElements parseFrom(byte[] data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ClassifiedElements)PARSER.parseFrom(data, extensionRegistry);
        }

        public static ClassifierOuterClass.ClassifiedElements parseFrom(InputStream input) throws IOException {
            return (ClassifierOuterClass.ClassifiedElements)GeneratedMessageV3.parseWithIOException(PARSER, input);
        }

        public static ClassifierOuterClass.ClassifiedElements parseFrom(InputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (ClassifierOuterClass.ClassifiedElements)GeneratedMessageV3.parseWithIOException(PARSER, input, extensionRegistry);
        }

        public static ClassifierOuterClass.ClassifiedElements parseDelimitedFrom(InputStream input) throws IOException {
            return (ClassifierOuterClass.ClassifiedElements)GeneratedMessageV3.parseDelimitedWithIOException(PARSER, input);
        }

        public static ClassifierOuterClass.ClassifiedElements parseDelimitedFrom(InputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (ClassifierOuterClass.ClassifiedElements)GeneratedMessageV3.parseDelimitedWithIOException(PARSER, input, extensionRegistry);
        }

        public static ClassifierOuterClass.ClassifiedElements parseFrom(CodedInputStream input) throws IOException {
            return (ClassifierOuterClass.ClassifiedElements)GeneratedMessageV3.parseWithIOException(PARSER, input);
        }

        public static ClassifierOuterClass.ClassifiedElements parseFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (ClassifierOuterClass.ClassifiedElements)GeneratedMessageV3.parseWithIOException(PARSER, input, extensionRegistry);
        }

        public ClassifierOuterClass.ClassifiedElements.Builder newBuilderForType() {
            return newBuilder();
        }

        public static ClassifierOuterClass.ClassifiedElements.Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static ClassifierOuterClass.ClassifiedElements.Builder newBuilder(ClassifierOuterClass.ClassifiedElements prototype) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
        }

        public ClassifierOuterClass.ClassifiedElements.Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new ClassifierOuterClass.ClassifiedElements.Builder() : (new ClassifierOuterClass.ClassifiedElements.Builder()).mergeFrom(this);
        }

        protected ClassifierOuterClass.ClassifiedElements.Builder newBuilderForType(BuilderParent parent) {
            ClassifierOuterClass.ClassifiedElements.Builder builder = new ClassifierOuterClass.ClassifiedElements.Builder(parent);
            return builder;
        }

        public static ClassifierOuterClass.ClassifiedElements getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<ClassifierOuterClass.ClassifiedElements> parser() {
            return PARSER;
        }

        public Parser<ClassifierOuterClass.ClassifiedElements> getParserForType() {
            return PARSER;
        }

        public ClassifierOuterClass.ClassifiedElements getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        public static final class Builder extends com.google.protobuf.GeneratedMessageV3.Builder<ClassifierOuterClass.ClassifiedElements.Builder> implements ClassifierOuterClass.ClassifiedElementsOrBuilder {
            private int bitField0_;
            private MapField<String, ClassifierOuterClass.ElementClassificationResult> classifications_;

            public static final Descriptor getDescriptor() {
                return ClassifierOuterClass.internal_static_ClassifiedElements_descriptor;
            }

            protected MapField internalGetMapField(int number) {
                switch(number) {
                    case 1:
                        return this.internalGetClassifications();
                    default:
                        throw new RuntimeException("Invalid map field number: " + number);
                }
            }

            protected MapField internalGetMutableMapField(int number) {
                switch(number) {
                    case 1:
                        return this.internalGetMutableClassifications();
                    default:
                        throw new RuntimeException("Invalid map field number: " + number);
                }
            }

            protected FieldAccessorTable internalGetFieldAccessorTable() {
                return ClassifierOuterClass.internal_static_ClassifiedElements_fieldAccessorTable.ensureFieldAccessorsInitialized(ClassifierOuterClass.ClassifiedElements.class, ClassifierOuterClass.ClassifiedElements.Builder.class);
            }

            private Builder() {
                this.maybeForceBuilderInitialization();
            }

            private Builder(BuilderParent parent) {
                super(parent);
                this.maybeForceBuilderInitialization();
            }

            private void maybeForceBuilderInitialization() {
                if (ClassifierOuterClass.ClassifiedElements.alwaysUseFieldBuilders) {
                }

            }

            public ClassifierOuterClass.ClassifiedElements.Builder clear() {
                super.clear();
                this.internalGetMutableClassifications().clear();
                return this;
            }

            public Descriptor getDescriptorForType() {
                return ClassifierOuterClass.internal_static_ClassifiedElements_descriptor;
            }

            public ClassifierOuterClass.ClassifiedElements getDefaultInstanceForType() {
                return ClassifierOuterClass.ClassifiedElements.getDefaultInstance();
            }

            public ClassifierOuterClass.ClassifiedElements build() {
                ClassifierOuterClass.ClassifiedElements result = this.buildPartial();
                if (!result.isInitialized()) {
                    throw newUninitializedMessageException(result);
                } else {
                    return result;
                }
            }

            public ClassifierOuterClass.ClassifiedElements buildPartial() {
                ClassifierOuterClass.ClassifiedElements result = new ClassifierOuterClass.ClassifiedElements(this);
                int from_bitField0_ = this.bitField0_;
                result.classifications_ = this.internalGetClassifications();
                result.classifications_.makeImmutable();
                this.onBuilt();
                return result;
            }

            public ClassifierOuterClass.ClassifiedElements.Builder clone() {
                return (ClassifierOuterClass.ClassifiedElements.Builder)super.clone();
            }

            public ClassifierOuterClass.ClassifiedElements.Builder setField(FieldDescriptor field, Object value) {
                return (ClassifierOuterClass.ClassifiedElements.Builder)super.setField(field, value);
            }

            public ClassifierOuterClass.ClassifiedElements.Builder clearField(FieldDescriptor field) {
                return (ClassifierOuterClass.ClassifiedElements.Builder)super.clearField(field);
            }

            public ClassifierOuterClass.ClassifiedElements.Builder clearOneof(OneofDescriptor oneof) {
                return (ClassifierOuterClass.ClassifiedElements.Builder)super.clearOneof(oneof);
            }

            public ClassifierOuterClass.ClassifiedElements.Builder setRepeatedField(FieldDescriptor field, int index, Object value) {
                return (ClassifierOuterClass.ClassifiedElements.Builder)super.setRepeatedField(field, index, value);
            }

            public ClassifierOuterClass.ClassifiedElements.Builder addRepeatedField(FieldDescriptor field, Object value) {
                return (ClassifierOuterClass.ClassifiedElements.Builder)super.addRepeatedField(field, value);
            }

            public ClassifierOuterClass.ClassifiedElements.Builder mergeFrom(Message other) {
                if (other instanceof ClassifierOuterClass.ClassifiedElements) {
                    return this.mergeFrom((ClassifierOuterClass.ClassifiedElements)other);
                } else {
                    super.mergeFrom(other);
                    return this;
                }
            }

            public ClassifierOuterClass.ClassifiedElements.Builder mergeFrom(ClassifierOuterClass.ClassifiedElements other) {
                if (other == ClassifierOuterClass.ClassifiedElements.getDefaultInstance()) {
                    return this;
                } else {
                    this.internalGetMutableClassifications().mergeFrom(other.internalGetClassifications());
                    this.mergeUnknownFields(other.unknownFields);
                    this.onChanged();
                    return this;
                }
            }

            public final boolean isInitialized() {
                return true;
            }

            public ClassifierOuterClass.ClassifiedElements.Builder mergeFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
                ClassifierOuterClass.ClassifiedElements parsedMessage = null;

                try {
                    parsedMessage = (ClassifierOuterClass.ClassifiedElements)ClassifierOuterClass.ClassifiedElements.PARSER.parsePartialFrom(input, extensionRegistry);
                } catch (InvalidProtocolBufferException var8) {
                    parsedMessage = (ClassifierOuterClass.ClassifiedElements)var8.getUnfinishedMessage();
                    throw var8.unwrapIOException();
                } finally {
                    if (parsedMessage != null) {
                        this.mergeFrom(parsedMessage);
                    }

                }

                return this;
            }

            private MapField<String, ClassifierOuterClass.ElementClassificationResult> internalGetClassifications() {
                return this.classifications_ == null ? MapField.emptyMapField(ClassifierOuterClass.ClassifiedElements.ClassificationsDefaultEntryHolder.defaultEntry) : this.classifications_;
            }

            private MapField<String, ClassifierOuterClass.ElementClassificationResult> internalGetMutableClassifications() {
                this.onChanged();
                if (this.classifications_ == null) {
                    this.classifications_ = MapField.newMapField(ClassifierOuterClass.ClassifiedElements.ClassificationsDefaultEntryHolder.defaultEntry);
                }

                if (!this.classifications_.isMutable()) {
                    this.classifications_ = this.classifications_.copy();
                }

                return this.classifications_;
            }

            public int getClassificationsCount() {
                return this.internalGetClassifications().getMap().size();
            }

            public boolean containsClassifications(String key) {
                if (key == null) {
                    throw new NullPointerException();
                } else {
                    return this.internalGetClassifications().getMap().containsKey(key);
                }
            }

            /** @deprecated */
            @Deprecated
            public Map<String, ClassifierOuterClass.ElementClassificationResult> getClassifications() {
                return this.getClassificationsMap();
            }

            public Map<String, ClassifierOuterClass.ElementClassificationResult> getClassificationsMap() {
                return this.internalGetClassifications().getMap();
            }

            public ClassifierOuterClass.ElementClassificationResult getClassificationsOrDefault(String key, ClassifierOuterClass.ElementClassificationResult defaultValue) {
                if (key == null) {
                    throw new NullPointerException();
                } else {
                    Map<String, ClassifierOuterClass.ElementClassificationResult> map = this.internalGetClassifications().getMap();
                    return map.containsKey(key) ? (ClassifierOuterClass.ElementClassificationResult)map.get(key) : defaultValue;
                }
            }

            public ClassifierOuterClass.ElementClassificationResult getClassificationsOrThrow(String key) {
                if (key == null) {
                    throw new NullPointerException();
                } else {
                    Map<String, ClassifierOuterClass.ElementClassificationResult> map = this.internalGetClassifications().getMap();
                    if (!map.containsKey(key)) {
                        throw new IllegalArgumentException();
                    } else {
                        return (ClassifierOuterClass.ElementClassificationResult)map.get(key);
                    }
                }
            }

            public ClassifierOuterClass.ClassifiedElements.Builder clearClassifications() {
                this.internalGetMutableClassifications().getMutableMap().clear();
                return this;
            }

            public ClassifierOuterClass.ClassifiedElements.Builder removeClassifications(String key) {
                if (key == null) {
                    throw new NullPointerException();
                } else {
                    this.internalGetMutableClassifications().getMutableMap().remove(key);
                    return this;
                }
            }

            /** @deprecated */
            @Deprecated
            public Map<String, ClassifierOuterClass.ElementClassificationResult> getMutableClassifications() {
                return this.internalGetMutableClassifications().getMutableMap();
            }

            public ClassifierOuterClass.ClassifiedElements.Builder putClassifications(String key, ClassifierOuterClass.ElementClassificationResult value) {
                if (key == null) {
                    throw new NullPointerException();
                } else if (value == null) {
                    throw new NullPointerException();
                } else {
                    this.internalGetMutableClassifications().getMutableMap().put(key, value);
                    return this;
                }
            }

            public ClassifierOuterClass.ClassifiedElements.Builder putAllClassifications(Map<String, ClassifierOuterClass.ElementClassificationResult> values) {
                this.internalGetMutableClassifications().getMutableMap().putAll(values);
                return this;
            }

            public final ClassifierOuterClass.ClassifiedElements.Builder setUnknownFields(UnknownFieldSet unknownFields) {
                return (ClassifierOuterClass.ClassifiedElements.Builder)super.setUnknownFields(unknownFields);
            }

            public final ClassifierOuterClass.ClassifiedElements.Builder mergeUnknownFields(UnknownFieldSet unknownFields) {
                return (ClassifierOuterClass.ClassifiedElements.Builder)super.mergeUnknownFields(unknownFields);
            }
        }

        private static final class ClassificationsDefaultEntryHolder {
            static final MapEntry<String, ClassifierOuterClass.ElementClassificationResult> defaultEntry;

            private ClassificationsDefaultEntryHolder() {
            }

            static {
                defaultEntry = MapEntry.newDefaultInstance(ClassifierOuterClass.internal_static_ClassifiedElements_ClassificationsEntry_descriptor, FieldType.STRING, "", FieldType.MESSAGE, ClassifierOuterClass.ElementClassificationResult.getDefaultInstance());
            }
        }
    }

    public interface ClassifiedElementsOrBuilder extends MessageOrBuilder {
        int getClassificationsCount();

        boolean containsClassifications(String var1);

        /** @deprecated */
        @Deprecated
        Map<String, ClassifierOuterClass.ElementClassificationResult> getClassifications();

        Map<String, ClassifierOuterClass.ElementClassificationResult> getClassificationsMap();

        ClassifierOuterClass.ElementClassificationResult getClassificationsOrDefault(String var1, ClassifierOuterClass.ElementClassificationResult var2);

        ClassifierOuterClass.ElementClassificationResult getClassificationsOrThrow(String var1);
    }

    public static final class ElementClassificationResult extends GeneratedMessageV3 implements ClassifierOuterClass.ElementClassificationResultOrBuilder {
        private static final long serialVersionUID = 0L;
        public static final int LABEL_FIELD_NUMBER = 1;
        private volatile Object label_;
        public static final int CONFIDENCE_FIELD_NUMBER = 2;
        private double confidence_;
        public static final int CONFIDENCEFORHINT_FIELD_NUMBER = 3;
        private double confidenceForHint_;
        private byte memoizedIsInitialized;
        private static final ClassifierOuterClass.ElementClassificationResult DEFAULT_INSTANCE = new ClassifierOuterClass.ElementClassificationResult();
        private static final Parser<ClassifierOuterClass.ElementClassificationResult> PARSER = new AbstractParser<ClassifierOuterClass.ElementClassificationResult>() {
            public ClassifierOuterClass.ElementClassificationResult parsePartialFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
                return new ClassifierOuterClass.ElementClassificationResult(input, extensionRegistry);
            }
        };

        private ElementClassificationResult(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
            super(builder);
            this.memoizedIsInitialized = -1;
        }

        private ElementClassificationResult() {
            this.memoizedIsInitialized = -1;
            this.label_ = "";
        }

        protected Object newInstance(UnusedPrivateParameter unused) {
            return new ClassifierOuterClass.ElementClassificationResult();
        }

        public final UnknownFieldSet getUnknownFields() {
            return this.unknownFields;
        }

        private ElementClassificationResult(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            this();
            if (extensionRegistry == null) {
                throw new NullPointerException();
            } else {
                com.google.protobuf.UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder();

                try {
                    boolean done = false;

                    while(!done) {
                        int tag = input.readTag();
                        switch(tag) {
                            case 0:
                                done = true;
                                break;
                            case 10:
                                String s = input.readStringRequireUtf8();
                                this.label_ = s;
                                break;
                            case 17:
                                this.confidence_ = input.readDouble();
                                break;
                            case 25:
                                this.confidenceForHint_ = input.readDouble();
                                break;
                            default:
                                if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                    done = true;
                                }
                        }
                    }
                } catch (InvalidProtocolBufferException var11) {
                    throw var11.setUnfinishedMessage(this);
                } catch (IOException var12) {
                    throw (new InvalidProtocolBufferException(var12)).setUnfinishedMessage(this);
                } finally {
                    this.unknownFields = unknownFields.build();
                    this.makeExtensionsImmutable();
                }

            }
        }

        public static final Descriptor getDescriptor() {
            return ClassifierOuterClass.internal_static_ElementClassificationResult_descriptor;
        }

        protected FieldAccessorTable internalGetFieldAccessorTable() {
            return ClassifierOuterClass.internal_static_ElementClassificationResult_fieldAccessorTable.ensureFieldAccessorsInitialized(ClassifierOuterClass.ElementClassificationResult.class, ClassifierOuterClass.ElementClassificationResult.Builder.class);
        }

        public String getLabel() {
            Object ref = this.label_;
            if (ref instanceof String) {
                return (String)ref;
            } else {
                ByteString bs = (ByteString)ref;
                String s = bs.toStringUtf8();
                this.label_ = s;
                return s;
            }
        }

        public ByteString getLabelBytes() {
            Object ref = this.label_;
            if (ref instanceof String) {
                ByteString b = ByteString.copyFromUtf8((String)ref);
                this.label_ = b;
                return b;
            } else {
                return (ByteString)ref;
            }
        }

        public double getConfidence() {
            return this.confidence_;
        }

        public double getConfidenceForHint() {
            return this.confidenceForHint_;
        }

        public final boolean isInitialized() {
            byte isInitialized = this.memoizedIsInitialized;
            if (isInitialized == 1) {
                return true;
            } else if (isInitialized == 0) {
                return false;
            } else {
                this.memoizedIsInitialized = 1;
                return true;
            }
        }

        public void writeTo(CodedOutputStream output) throws IOException {
            if (!this.getLabelBytes().isEmpty()) {
                GeneratedMessageV3.writeString(output, 1, this.label_);
            }

            if (this.confidence_ != 0.0D) {
                output.writeDouble(2, this.confidence_);
            }

            if (this.confidenceForHint_ != 0.0D) {
                output.writeDouble(3, this.confidenceForHint_);
            }

            this.unknownFields.writeTo(output);
        }

        public int getSerializedSize() {
            int size = this.memoizedSize;
            if (size != -1) {
                return size;
            } else {
                size = 0;
                if (!this.getLabelBytes().isEmpty()) {
                    size += GeneratedMessageV3.computeStringSize(1, this.label_);
                }

                if (this.confidence_ != 0.0D) {
                    size += CodedOutputStream.computeDoubleSize(2, this.confidence_);
                }

                if (this.confidenceForHint_ != 0.0D) {
                    size += CodedOutputStream.computeDoubleSize(3, this.confidenceForHint_);
                }

                size += this.unknownFields.getSerializedSize();
                this.memoizedSize = size;
                return size;
            }
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            } else if (!(obj instanceof ClassifierOuterClass.ElementClassificationResult)) {
                return super.equals(obj);
            } else {
                ClassifierOuterClass.ElementClassificationResult other = (ClassifierOuterClass.ElementClassificationResult)obj;
                if (!this.getLabel().equals(other.getLabel())) {
                    return false;
                } else if (Double.doubleToLongBits(this.getConfidence()) != Double.doubleToLongBits(other.getConfidence())) {
                    return false;
                } else if (Double.doubleToLongBits(this.getConfidenceForHint()) != Double.doubleToLongBits(other.getConfidenceForHint())) {
                    return false;
                } else {
                    return this.unknownFields.equals(other.unknownFields);
                }
            }
        }

        public int hashCode() {
            if (this.memoizedHashCode != 0) {
                return this.memoizedHashCode;
            } else {
                int hash = 41;
                hash = 19 * hash + getDescriptor().hashCode();
                hash = 37 * hash + 1;
                hash = 53 * hash + this.getLabel().hashCode();
                hash = 37 * hash + 2;
                hash = 53 * hash + Internal.hashLong(Double.doubleToLongBits(this.getConfidence()));
                hash = 37 * hash + 3;
                hash = 53 * hash + Internal.hashLong(Double.doubleToLongBits(this.getConfidenceForHint()));
                hash = 29 * hash + this.unknownFields.hashCode();
                this.memoizedHashCode = hash;
                return hash;
            }
        }

        public static ClassifierOuterClass.ElementClassificationResult parseFrom(ByteBuffer data) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ElementClassificationResult)PARSER.parseFrom(data);
        }

        public static ClassifierOuterClass.ElementClassificationResult parseFrom(ByteBuffer data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ElementClassificationResult)PARSER.parseFrom(data, extensionRegistry);
        }

        public static ClassifierOuterClass.ElementClassificationResult parseFrom(ByteString data) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ElementClassificationResult)PARSER.parseFrom(data);
        }

        public static ClassifierOuterClass.ElementClassificationResult parseFrom(ByteString data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ElementClassificationResult)PARSER.parseFrom(data, extensionRegistry);
        }

        public static ClassifierOuterClass.ElementClassificationResult parseFrom(byte[] data) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ElementClassificationResult)PARSER.parseFrom(data);
        }

        public static ClassifierOuterClass.ElementClassificationResult parseFrom(byte[] data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ElementClassificationResult)PARSER.parseFrom(data, extensionRegistry);
        }

        public static ClassifierOuterClass.ElementClassificationResult parseFrom(InputStream input) throws IOException {
            return (ClassifierOuterClass.ElementClassificationResult)GeneratedMessageV3.parseWithIOException(PARSER, input);
        }

        public static ClassifierOuterClass.ElementClassificationResult parseFrom(InputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (ClassifierOuterClass.ElementClassificationResult)GeneratedMessageV3.parseWithIOException(PARSER, input, extensionRegistry);
        }

        public static ClassifierOuterClass.ElementClassificationResult parseDelimitedFrom(InputStream input) throws IOException {
            return (ClassifierOuterClass.ElementClassificationResult)GeneratedMessageV3.parseDelimitedWithIOException(PARSER, input);
        }

        public static ClassifierOuterClass.ElementClassificationResult parseDelimitedFrom(InputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (ClassifierOuterClass.ElementClassificationResult)GeneratedMessageV3.parseDelimitedWithIOException(PARSER, input, extensionRegistry);
        }

        public static ClassifierOuterClass.ElementClassificationResult parseFrom(CodedInputStream input) throws IOException {
            return (ClassifierOuterClass.ElementClassificationResult)GeneratedMessageV3.parseWithIOException(PARSER, input);
        }

        public static ClassifierOuterClass.ElementClassificationResult parseFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (ClassifierOuterClass.ElementClassificationResult)GeneratedMessageV3.parseWithIOException(PARSER, input, extensionRegistry);
        }

        public ClassifierOuterClass.ElementClassificationResult.Builder newBuilderForType() {
            return newBuilder();
        }

        public static ClassifierOuterClass.ElementClassificationResult.Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static ClassifierOuterClass.ElementClassificationResult.Builder newBuilder(ClassifierOuterClass.ElementClassificationResult prototype) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
        }

        public ClassifierOuterClass.ElementClassificationResult.Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new ClassifierOuterClass.ElementClassificationResult.Builder() : (new ClassifierOuterClass.ElementClassificationResult.Builder()).mergeFrom(this);
        }

        protected ClassifierOuterClass.ElementClassificationResult.Builder newBuilderForType(BuilderParent parent) {
            ClassifierOuterClass.ElementClassificationResult.Builder builder = new ClassifierOuterClass.ElementClassificationResult.Builder(parent);
            return builder;
        }

        public static ClassifierOuterClass.ElementClassificationResult getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<ClassifierOuterClass.ElementClassificationResult> parser() {
            return PARSER;
        }

        public Parser<ClassifierOuterClass.ElementClassificationResult> getParserForType() {
            return PARSER;
        }

        public ClassifierOuterClass.ElementClassificationResult getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        public static final class Builder extends com.google.protobuf.GeneratedMessageV3.Builder<ClassifierOuterClass.ElementClassificationResult.Builder> implements ClassifierOuterClass.ElementClassificationResultOrBuilder {
            private Object label_;
            private double confidence_;
            private double confidenceForHint_;

            public static final Descriptor getDescriptor() {
                return ClassifierOuterClass.internal_static_ElementClassificationResult_descriptor;
            }

            protected FieldAccessorTable internalGetFieldAccessorTable() {
                return ClassifierOuterClass.internal_static_ElementClassificationResult_fieldAccessorTable.ensureFieldAccessorsInitialized(ClassifierOuterClass.ElementClassificationResult.class, ClassifierOuterClass.ElementClassificationResult.Builder.class);
            }

            private Builder() {
                this.label_ = "";
                this.maybeForceBuilderInitialization();
            }

            private Builder(BuilderParent parent) {
                super(parent);
                this.label_ = "";
                this.maybeForceBuilderInitialization();
            }

            private void maybeForceBuilderInitialization() {
                if (ClassifierOuterClass.ElementClassificationResult.alwaysUseFieldBuilders) {
                }

            }

            public ClassifierOuterClass.ElementClassificationResult.Builder clear() {
                super.clear();
                this.label_ = "";
                this.confidence_ = 0.0D;
                this.confidenceForHint_ = 0.0D;
                return this;
            }

            public Descriptor getDescriptorForType() {
                return ClassifierOuterClass.internal_static_ElementClassificationResult_descriptor;
            }

            public ClassifierOuterClass.ElementClassificationResult getDefaultInstanceForType() {
                return ClassifierOuterClass.ElementClassificationResult.getDefaultInstance();
            }

            public ClassifierOuterClass.ElementClassificationResult build() {
                ClassifierOuterClass.ElementClassificationResult result = this.buildPartial();
                if (!result.isInitialized()) {
                    throw newUninitializedMessageException(result);
                } else {
                    return result;
                }
            }

            public ClassifierOuterClass.ElementClassificationResult buildPartial() {
                ClassifierOuterClass.ElementClassificationResult result = new ClassifierOuterClass.ElementClassificationResult(this);
                result.label_ = this.label_;
                result.confidence_ = this.confidence_;
                result.confidenceForHint_ = this.confidenceForHint_;
                this.onBuilt();
                return result;
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder clone() {
                return (ClassifierOuterClass.ElementClassificationResult.Builder)super.clone();
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder setField(FieldDescriptor field, Object value) {
                return (ClassifierOuterClass.ElementClassificationResult.Builder)super.setField(field, value);
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder clearField(FieldDescriptor field) {
                return (ClassifierOuterClass.ElementClassificationResult.Builder)super.clearField(field);
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder clearOneof(OneofDescriptor oneof) {
                return (ClassifierOuterClass.ElementClassificationResult.Builder)super.clearOneof(oneof);
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder setRepeatedField(FieldDescriptor field, int index, Object value) {
                return (ClassifierOuterClass.ElementClassificationResult.Builder)super.setRepeatedField(field, index, value);
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder addRepeatedField(FieldDescriptor field, Object value) {
                return (ClassifierOuterClass.ElementClassificationResult.Builder)super.addRepeatedField(field, value);
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder mergeFrom(Message other) {
                if (other instanceof ClassifierOuterClass.ElementClassificationResult) {
                    return this.mergeFrom((ClassifierOuterClass.ElementClassificationResult)other);
                } else {
                    super.mergeFrom(other);
                    return this;
                }
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder mergeFrom(ClassifierOuterClass.ElementClassificationResult other) {
                if (other == ClassifierOuterClass.ElementClassificationResult.getDefaultInstance()) {
                    return this;
                } else {
                    if (!other.getLabel().isEmpty()) {
                        this.label_ = other.label_;
                        this.onChanged();
                    }

                    if (other.getConfidence() != 0.0D) {
                        this.setConfidence(other.getConfidence());
                    }

                    if (other.getConfidenceForHint() != 0.0D) {
                        this.setConfidenceForHint(other.getConfidenceForHint());
                    }

                    this.mergeUnknownFields(other.unknownFields);
                    this.onChanged();
                    return this;
                }
            }

            public final boolean isInitialized() {
                return true;
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder mergeFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
                ClassifierOuterClass.ElementClassificationResult parsedMessage = null;

                try {
                    parsedMessage = (ClassifierOuterClass.ElementClassificationResult)ClassifierOuterClass.ElementClassificationResult.PARSER.parsePartialFrom(input, extensionRegistry);
                } catch (InvalidProtocolBufferException var8) {
                    parsedMessage = (ClassifierOuterClass.ElementClassificationResult)var8.getUnfinishedMessage();
                    throw var8.unwrapIOException();
                } finally {
                    if (parsedMessage != null) {
                        this.mergeFrom(parsedMessage);
                    }

                }

                return this;
            }

            public String getLabel() {
                Object ref = this.label_;
                if (!(ref instanceof String)) {
                    ByteString bs = (ByteString)ref;
                    String s = bs.toStringUtf8();
                    this.label_ = s;
                    return s;
                } else {
                    return (String)ref;
                }
            }

            public ByteString getLabelBytes() {
                Object ref = this.label_;
                if (ref instanceof String) {
                    ByteString b = ByteString.copyFromUtf8((String)ref);
                    this.label_ = b;
                    return b;
                } else {
                    return (ByteString)ref;
                }
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder setLabel(String value) {
                if (value == null) {
                    throw new NullPointerException();
                } else {
                    this.label_ = value;
                    this.onChanged();
                    return this;
                }
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder clearLabel() {
                this.label_ = ClassifierOuterClass.ElementClassificationResult.getDefaultInstance().getLabel();
                this.onChanged();
                return this;
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder setLabelBytes(ByteString value) {
                if (value == null) {
                    throw new NullPointerException();
                } else {
                    ClassifierOuterClass.ElementClassificationResult.checkByteStringIsUtf8(value);
                    this.label_ = value;
                    this.onChanged();
                    return this;
                }
            }

            public double getConfidence() {
                return this.confidence_;
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder setConfidence(double value) {
                this.confidence_ = value;
                this.onChanged();
                return this;
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder clearConfidence() {
                this.confidence_ = 0.0D;
                this.onChanged();
                return this;
            }

            public double getConfidenceForHint() {
                return this.confidenceForHint_;
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder setConfidenceForHint(double value) {
                this.confidenceForHint_ = value;
                this.onChanged();
                return this;
            }

            public ClassifierOuterClass.ElementClassificationResult.Builder clearConfidenceForHint() {
                this.confidenceForHint_ = 0.0D;
                this.onChanged();
                return this;
            }

            public final ClassifierOuterClass.ElementClassificationResult.Builder setUnknownFields(UnknownFieldSet unknownFields) {
                return (ClassifierOuterClass.ElementClassificationResult.Builder)super.setUnknownFields(unknownFields);
            }

            public final ClassifierOuterClass.ElementClassificationResult.Builder mergeUnknownFields(UnknownFieldSet unknownFields) {
                return (ClassifierOuterClass.ElementClassificationResult.Builder)super.mergeUnknownFields(unknownFields);
            }
        }
    }

    public interface ElementClassificationResultOrBuilder extends MessageOrBuilder {
        String getLabel();

        ByteString getLabelBytes();

        double getConfidence();

        double getConfidenceForHint();
    }

    public static final class ElementClassificationRequest extends GeneratedMessageV3 implements ClassifierOuterClass.ElementClassificationRequestOrBuilder {
        private static final long serialVersionUID = 0L;
        public static final int LABELHINT_FIELD_NUMBER = 1;
        private volatile Object labelHint_;
        public static final int ELEMENTIMAGES_FIELD_NUMBER = 2;
        private MapField<String, ByteString> elementImages_;
        public static final int CONFIDENCETHRESHOLD_FIELD_NUMBER = 3;
        private double confidenceThreshold_;
        public static final int ALLOWWEAKERMATCHES_FIELD_NUMBER = 4;
        private boolean allowWeakerMatches_;
        private byte memoizedIsInitialized;
        private static final ClassifierOuterClass.ElementClassificationRequest DEFAULT_INSTANCE = new ClassifierOuterClass.ElementClassificationRequest();
        private static final Parser<ClassifierOuterClass.ElementClassificationRequest> PARSER = new AbstractParser<ClassifierOuterClass.ElementClassificationRequest>() {
            public ClassifierOuterClass.ElementClassificationRequest parsePartialFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
                return new ClassifierOuterClass.ElementClassificationRequest(input, extensionRegistry);
            }
        };

        private ElementClassificationRequest(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
            super(builder);
            this.memoizedIsInitialized = -1;
        }

        private ElementClassificationRequest() {
            this.memoizedIsInitialized = -1;
            this.labelHint_ = "";
        }

        protected Object newInstance(UnusedPrivateParameter unused) {
            return new ClassifierOuterClass.ElementClassificationRequest();
        }

        public final UnknownFieldSet getUnknownFields() {
            return this.unknownFields;
        }

        private ElementClassificationRequest(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            this();
            if (extensionRegistry == null) {
                throw new NullPointerException();
            } else {
                boolean mutable_bitField0_ = false;
                com.google.protobuf.UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder();

                try {
                    boolean done = false;

                    while(!done) {
                        int tag = input.readTag();
                        switch(tag) {
                            case 0:
                                done = true;
                                break;
                            case 10:
                                String s = input.readStringRequireUtf8();
                                this.labelHint_ = s;
                                break;
                            case 18:
                                if (!(mutable_bitField0_ & true)) {
                                    this.elementImages_ = MapField.newMapField(ClassifierOuterClass.ElementClassificationRequest.ElementImagesDefaultEntryHolder.defaultEntry);
                                    mutable_bitField0_ |= true;
                                }

                                MapEntry<String, ByteString> elementImages__ = (MapEntry)input.readMessage(ClassifierOuterClass.ElementClassificationRequest.ElementImagesDefaultEntryHolder.defaultEntry.getParserForType(), extensionRegistry);
                                this.elementImages_.getMutableMap().put(elementImages__.getKey(), elementImages__.getValue());
                                break;
                            case 25:
                                this.confidenceThreshold_ = input.readDouble();
                                break;
                            case 32:
                                this.allowWeakerMatches_ = input.readBool();
                                break;
                            default:
                                if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                    done = true;
                                }
                        }
                    }
                } catch (InvalidProtocolBufferException var12) {
                    throw var12.setUnfinishedMessage(this);
                } catch (IOException var13) {
                    throw (new InvalidProtocolBufferException(var13)).setUnfinishedMessage(this);
                } finally {
                    this.unknownFields = unknownFields.build();
                    this.makeExtensionsImmutable();
                }

            }
        }

        public static final Descriptor getDescriptor() {
            return ClassifierOuterClass.internal_static_ElementClassificationRequest_descriptor;
        }

        protected MapField internalGetMapField(int number) {
            switch(number) {
                case 2:
                    return this.internalGetElementImages();
                default:
                    throw new RuntimeException("Invalid map field number: " + number);
            }
        }

        protected FieldAccessorTable internalGetFieldAccessorTable() {
            return ClassifierOuterClass.internal_static_ElementClassificationRequest_fieldAccessorTable.ensureFieldAccessorsInitialized(ClassifierOuterClass.ElementClassificationRequest.class, ClassifierOuterClass.ElementClassificationRequest.Builder.class);
        }

        public String getLabelHint() {
            Object ref = this.labelHint_;
            if (ref instanceof String) {
                return (String)ref;
            } else {
                ByteString bs = (ByteString)ref;
                String s = bs.toStringUtf8();
                this.labelHint_ = s;
                return s;
            }
        }

        public ByteString getLabelHintBytes() {
            Object ref = this.labelHint_;
            if (ref instanceof String) {
                ByteString b = ByteString.copyFromUtf8((String)ref);
                this.labelHint_ = b;
                return b;
            } else {
                return (ByteString)ref;
            }
        }

        private MapField<String, ByteString> internalGetElementImages() {
            return this.elementImages_ == null ? MapField.emptyMapField(ClassifierOuterClass.ElementClassificationRequest.ElementImagesDefaultEntryHolder.defaultEntry) : this.elementImages_;
        }

        public int getElementImagesCount() {
            return this.internalGetElementImages().getMap().size();
        }

        public boolean containsElementImages(String key) {
            if (key == null) {
                throw new NullPointerException();
            } else {
                return this.internalGetElementImages().getMap().containsKey(key);
            }
        }

        /** @deprecated */
        @Deprecated
        public Map<String, ByteString> getElementImages() {
            return this.getElementImagesMap();
        }

        public Map<String, ByteString> getElementImagesMap() {
            return this.internalGetElementImages().getMap();
        }

        public ByteString getElementImagesOrDefault(String key, ByteString defaultValue) {
            if (key == null) {
                throw new NullPointerException();
            } else {
                Map<String, ByteString> map = this.internalGetElementImages().getMap();
                return map.containsKey(key) ? (ByteString)map.get(key) : defaultValue;
            }
        }

        public ByteString getElementImagesOrThrow(String key) {
            if (key == null) {
                throw new NullPointerException();
            } else {
                Map<String, ByteString> map = this.internalGetElementImages().getMap();
                if (!map.containsKey(key)) {
                    throw new IllegalArgumentException();
                } else {
                    return (ByteString)map.get(key);
                }
            }
        }

        public double getConfidenceThreshold() {
            return this.confidenceThreshold_;
        }

        public boolean getAllowWeakerMatches() {
            return this.allowWeakerMatches_;
        }

        public final boolean isInitialized() {
            byte isInitialized = this.memoizedIsInitialized;
            if (isInitialized == 1) {
                return true;
            } else if (isInitialized == 0) {
                return false;
            } else {
                this.memoizedIsInitialized = 1;
                return true;
            }
        }

        public void writeTo(CodedOutputStream output) throws IOException {
            if (!this.getLabelHintBytes().isEmpty()) {
                GeneratedMessageV3.writeString(output, 1, this.labelHint_);
            }

            GeneratedMessageV3.serializeStringMapTo(output, this.internalGetElementImages(), ClassifierOuterClass.ElementClassificationRequest.ElementImagesDefaultEntryHolder.defaultEntry, 2);
            if (this.confidenceThreshold_ != 0.0D) {
                output.writeDouble(3, this.confidenceThreshold_);
            }

            if (this.allowWeakerMatches_) {
                output.writeBool(4, this.allowWeakerMatches_);
            }

            this.unknownFields.writeTo(output);
        }

        public int getSerializedSize() {
            int size = this.memoizedSize;
            if (size != -1) {
                return size;
            } else {
                size = 0;
                if (!this.getLabelHintBytes().isEmpty()) {
                    size += GeneratedMessageV3.computeStringSize(1, this.labelHint_);
                }

                MapEntry elementImages__;
                for(Iterator var2 = this.internalGetElementImages().getMap().entrySet().iterator(); var2.hasNext(); size += CodedOutputStream.computeMessageSize(2, elementImages__)) {
                    Entry<String, ByteString> entry = (Entry)var2.next();
                    elementImages__ = ClassifierOuterClass.ElementClassificationRequest.ElementImagesDefaultEntryHolder.defaultEntry.newBuilderForType().setKey(entry.getKey()).setValue(entry.getValue()).build();
                }

                if (this.confidenceThreshold_ != 0.0D) {
                    size += CodedOutputStream.computeDoubleSize(3, this.confidenceThreshold_);
                }

                if (this.allowWeakerMatches_) {
                    size += CodedOutputStream.computeBoolSize(4, this.allowWeakerMatches_);
                }

                size += this.unknownFields.getSerializedSize();
                this.memoizedSize = size;
                return size;
            }
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            } else if (!(obj instanceof ClassifierOuterClass.ElementClassificationRequest)) {
                return super.equals(obj);
            } else {
                ClassifierOuterClass.ElementClassificationRequest other = (ClassifierOuterClass.ElementClassificationRequest)obj;
                if (!this.getLabelHint().equals(other.getLabelHint())) {
                    return false;
                } else if (!this.internalGetElementImages().equals(other.internalGetElementImages())) {
                    return false;
                } else if (Double.doubleToLongBits(this.getConfidenceThreshold()) != Double.doubleToLongBits(other.getConfidenceThreshold())) {
                    return false;
                } else if (this.getAllowWeakerMatches() != other.getAllowWeakerMatches()) {
                    return false;
                } else {
                    return this.unknownFields.equals(other.unknownFields);
                }
            }
        }

        public int hashCode() {
            if (this.memoizedHashCode != 0) {
                return this.memoizedHashCode;
            } else {
                int hash = 41;
                hash = 19 * hash + getDescriptor().hashCode();
                hash = 37 * hash + 1;
                hash = 53 * hash + this.getLabelHint().hashCode();
                if (!this.internalGetElementImages().getMap().isEmpty()) {
                    hash = 37 * hash + 2;
                    hash = 53 * hash + this.internalGetElementImages().hashCode();
                }

                hash = 37 * hash + 3;
                hash = 53 * hash + Internal.hashLong(Double.doubleToLongBits(this.getConfidenceThreshold()));
                hash = 37 * hash + 4;
                hash = 53 * hash + Internal.hashBoolean(this.getAllowWeakerMatches());
                hash = 29 * hash + this.unknownFields.hashCode();
                this.memoizedHashCode = hash;
                return hash;
            }
        }

        public static ClassifierOuterClass.ElementClassificationRequest parseFrom(ByteBuffer data) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ElementClassificationRequest)PARSER.parseFrom(data);
        }

        public static ClassifierOuterClass.ElementClassificationRequest parseFrom(ByteBuffer data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ElementClassificationRequest)PARSER.parseFrom(data, extensionRegistry);
        }

        public static ClassifierOuterClass.ElementClassificationRequest parseFrom(ByteString data) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ElementClassificationRequest)PARSER.parseFrom(data);
        }

        public static ClassifierOuterClass.ElementClassificationRequest parseFrom(ByteString data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ElementClassificationRequest)PARSER.parseFrom(data, extensionRegistry);
        }

        public static ClassifierOuterClass.ElementClassificationRequest parseFrom(byte[] data) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ElementClassificationRequest)PARSER.parseFrom(data);
        }

        public static ClassifierOuterClass.ElementClassificationRequest parseFrom(byte[] data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (ClassifierOuterClass.ElementClassificationRequest)PARSER.parseFrom(data, extensionRegistry);
        }

        public static ClassifierOuterClass.ElementClassificationRequest parseFrom(InputStream input) throws IOException {
            return (ClassifierOuterClass.ElementClassificationRequest)GeneratedMessageV3.parseWithIOException(PARSER, input);
        }

        public static ClassifierOuterClass.ElementClassificationRequest parseFrom(InputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (ClassifierOuterClass.ElementClassificationRequest)GeneratedMessageV3.parseWithIOException(PARSER, input, extensionRegistry);
        }

        public static ClassifierOuterClass.ElementClassificationRequest parseDelimitedFrom(InputStream input) throws IOException {
            return (ClassifierOuterClass.ElementClassificationRequest)GeneratedMessageV3.parseDelimitedWithIOException(PARSER, input);
        }

        public static ClassifierOuterClass.ElementClassificationRequest parseDelimitedFrom(InputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (ClassifierOuterClass.ElementClassificationRequest)GeneratedMessageV3.parseDelimitedWithIOException(PARSER, input, extensionRegistry);
        }

        public static ClassifierOuterClass.ElementClassificationRequest parseFrom(CodedInputStream input) throws IOException {
            return (ClassifierOuterClass.ElementClassificationRequest)GeneratedMessageV3.parseWithIOException(PARSER, input);
        }

        public static ClassifierOuterClass.ElementClassificationRequest parseFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (ClassifierOuterClass.ElementClassificationRequest)GeneratedMessageV3.parseWithIOException(PARSER, input, extensionRegistry);
        }

        public ClassifierOuterClass.ElementClassificationRequest.Builder newBuilderForType() {
            return newBuilder();
        }

        public static ClassifierOuterClass.ElementClassificationRequest.Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static ClassifierOuterClass.ElementClassificationRequest.Builder newBuilder(ClassifierOuterClass.ElementClassificationRequest prototype) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
        }

        public ClassifierOuterClass.ElementClassificationRequest.Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new ClassifierOuterClass.ElementClassificationRequest.Builder() : (new ClassifierOuterClass.ElementClassificationRequest.Builder()).mergeFrom(this);
        }

        protected ClassifierOuterClass.ElementClassificationRequest.Builder newBuilderForType(BuilderParent parent) {
            ClassifierOuterClass.ElementClassificationRequest.Builder builder = new ClassifierOuterClass.ElementClassificationRequest.Builder(parent);
            return builder;
        }

        public static ClassifierOuterClass.ElementClassificationRequest getDefaultInstance() {
            return  DEFAULT_INSTANCE;
        }

        public static Parser<ClassifierOuterClass.ElementClassificationRequest> parser() {
            return PARSER;
        }

        public Parser<ClassifierOuterClass.ElementClassificationRequest> getParserForType() {
            return PARSER;
        }

        public ClassifierOuterClass.ElementClassificationRequest getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        public static final class Builder extends com.google.protobuf.GeneratedMessageV3.Builder<ClassifierOuterClass.ElementClassificationRequest.Builder> implements ClassifierOuterClass.ElementClassificationRequestOrBuilder {
            private int bitField0_;
            private Object labelHint_;
            private MapField<String, ByteString> elementImages_;
            private double confidenceThreshold_;
            private boolean allowWeakerMatches_;

            public static final Descriptor getDescriptor() {
                return ClassifierOuterClass.internal_static_ElementClassificationRequest_descriptor;
            }

            protected MapField internalGetMapField(int number) {
                switch(number) {
                    case 2:
                        return this.internalGetElementImages();
                    default:
                        throw new RuntimeException("Invalid map field number: " + number);
                }
            }

            protected MapField internalGetMutableMapField(int number) {
                switch(number) {
                    case 2:
                        return this.internalGetMutableElementImages();
                    default:
                        throw new RuntimeException("Invalid map field number: " + number);
                }
            }

            protected FieldAccessorTable internalGetFieldAccessorTable() {
                return ClassifierOuterClass.internal_static_ElementClassificationRequest_fieldAccessorTable.ensureFieldAccessorsInitialized(ClassifierOuterClass.ElementClassificationRequest.class, ClassifierOuterClass.ElementClassificationRequest.Builder.class);
            }

            private Builder() {
                this.labelHint_ = "";
                this.maybeForceBuilderInitialization();
            }

            private Builder(BuilderParent parent) {
                super(parent);
                this.labelHint_ = "";
                this.maybeForceBuilderInitialization();
            }

            private void maybeForceBuilderInitialization() {
                if (ClassifierOuterClass.ElementClassificationRequest.alwaysUseFieldBuilders) {
                }

            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder clear() {
                super.clear();
                this.labelHint_ = "";
                this.internalGetMutableElementImages().clear();
                this.confidenceThreshold_ = 0.0D;
                this.allowWeakerMatches_ = false;
                return this;
            }

            public Descriptor getDescriptorForType() {
                return ClassifierOuterClass.internal_static_ElementClassificationRequest_descriptor;
            }

            public ClassifierOuterClass.ElementClassificationRequest getDefaultInstanceForType() {
                return ClassifierOuterClass.ElementClassificationRequest.getDefaultInstance();
            }

            public ClassifierOuterClass.ElementClassificationRequest build() {
                ClassifierOuterClass.ElementClassificationRequest result = this.buildPartial();
                if (!result.isInitialized()) {
                    throw newUninitializedMessageException(result);
                } else {
                    return result;
                }
            }

            public ClassifierOuterClass.ElementClassificationRequest buildPartial() {
                ClassifierOuterClass.ElementClassificationRequest result = new ClassifierOuterClass.ElementClassificationRequest(this);
                int from_bitField0_ = this.bitField0_;
                result.labelHint_ = this.labelHint_;
                result.elementImages_ = this.internalGetElementImages();
                result.elementImages_.makeImmutable();
                result.confidenceThreshold_ = this.confidenceThreshold_;
                result.allowWeakerMatches_ = this.allowWeakerMatches_;
                this.onBuilt();
                return result;
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder clone() {
                return (ClassifierOuterClass.ElementClassificationRequest.Builder)super.clone();
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder setField(FieldDescriptor field, Object value) {
                return (ClassifierOuterClass.ElementClassificationRequest.Builder)super.setField(field, value);
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder clearField(FieldDescriptor field) {
                return (ClassifierOuterClass.ElementClassificationRequest.Builder)super.clearField(field);
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder clearOneof(OneofDescriptor oneof) {
                return (ClassifierOuterClass.ElementClassificationRequest.Builder)super.clearOneof(oneof);
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder setRepeatedField(FieldDescriptor field, int index, Object value) {
                return (ClassifierOuterClass.ElementClassificationRequest.Builder)super.setRepeatedField(field, index, value);
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder addRepeatedField(FieldDescriptor field, Object value) {
                return (ClassifierOuterClass.ElementClassificationRequest.Builder)super.addRepeatedField(field, value);
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder mergeFrom(Message other) {
                if (other instanceof ClassifierOuterClass.ElementClassificationRequest) {
                    return this.mergeFrom((ClassifierOuterClass.ElementClassificationRequest)other);
                } else {
                    super.mergeFrom(other);
                    return this;
                }
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder mergeFrom(ClassifierOuterClass.ElementClassificationRequest other) {
                if (other == ClassifierOuterClass.ElementClassificationRequest.getDefaultInstance()) {
                    return this;
                } else {
                    if (!other.getLabelHint().isEmpty()) {
                        this.labelHint_ = other.labelHint_;
                        this.onChanged();
                    }

                    this.internalGetMutableElementImages().mergeFrom(other.internalGetElementImages());
                    if (other.getConfidenceThreshold() != 0.0D) {
                        this.setConfidenceThreshold(other.getConfidenceThreshold());
                    }

                    if (other.getAllowWeakerMatches()) {
                        this.setAllowWeakerMatches(other.getAllowWeakerMatches());
                    }

                    this.mergeUnknownFields(other.unknownFields);
                    this.onChanged();
                    return this;
                }
            }

            public final boolean isInitialized() {
                return true;
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder mergeFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
                ClassifierOuterClass.ElementClassificationRequest parsedMessage = null;

                try {
                    parsedMessage = (ClassifierOuterClass.ElementClassificationRequest)ClassifierOuterClass.ElementClassificationRequest.PARSER.parsePartialFrom(input, extensionRegistry);
                } catch (InvalidProtocolBufferException var8) {
                    parsedMessage = (ClassifierOuterClass.ElementClassificationRequest)var8.getUnfinishedMessage();
                    throw var8.unwrapIOException();
                } finally {
                    if (parsedMessage != null) {
                        this.mergeFrom(parsedMessage);
                    }

                }

                return this;
            }

            public String getLabelHint() {
                Object ref = this.labelHint_;
                if (!(ref instanceof String)) {
                    ByteString bs = (ByteString)ref;
                    String s = bs.toStringUtf8();
                    this.labelHint_ = s;
                    return s;
                } else {
                    return (String)ref;
                }
            }

            public ByteString getLabelHintBytes() {
                Object ref = this.labelHint_;
                if (ref instanceof String) {
                    ByteString b = ByteString.copyFromUtf8((String)ref);
                    this.labelHint_ = b;
                    return b;
                } else {
                    return (ByteString)ref;
                }
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder setLabelHint(String value) {
                if (value == null) {
                    throw new NullPointerException();
                } else {
                    this.labelHint_ = value;
                    this.onChanged();
                    return this;
                }
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder clearLabelHint() {
                this.labelHint_ = ClassifierOuterClass.ElementClassificationRequest.getDefaultInstance().getLabelHint();
                this.onChanged();
                return this;
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder setLabelHintBytes(ByteString value) {
                if (value == null) {
                    throw new NullPointerException();
                } else {
                    ClassifierOuterClass.ElementClassificationRequest.checkByteStringIsUtf8(value);
                    this.labelHint_ = value;
                    this.onChanged();
                    return this;
                }
            }

            private MapField<String, ByteString> internalGetElementImages() {
                return this.elementImages_ == null ? MapField.emptyMapField(ClassifierOuterClass.ElementClassificationRequest.ElementImagesDefaultEntryHolder.defaultEntry) : this.elementImages_;
            }

            private MapField<String, ByteString> internalGetMutableElementImages() {
                this.onChanged();
                if (this.elementImages_ == null) {
                    this.elementImages_ = MapField.newMapField(ClassifierOuterClass.ElementClassificationRequest.ElementImagesDefaultEntryHolder.defaultEntry);
                }

                if (!this.elementImages_.isMutable()) {
                    this.elementImages_ = this.elementImages_.copy();
                }

                return this.elementImages_;
            }

            public int getElementImagesCount() {
                return this.internalGetElementImages().getMap().size();
            }

            public boolean containsElementImages(String key) {
                if (key == null) {
                    throw new NullPointerException();
                } else {
                    return this.internalGetElementImages().getMap().containsKey(key);
                }
            }

            /** @deprecated */
            @Deprecated
            public Map<String, ByteString> getElementImages() {
                return this.getElementImagesMap();
            }

            public Map<String, ByteString> getElementImagesMap() {
                return this.internalGetElementImages().getMap();
            }

            public ByteString getElementImagesOrDefault(String key, ByteString defaultValue) {
                if (key == null) {
                    throw new NullPointerException();
                } else {
                    Map<String, ByteString> map = this.internalGetElementImages().getMap();
                    return map.containsKey(key) ? (ByteString)map.get(key) : defaultValue;
                }
            }

            public ByteString getElementImagesOrThrow(String key) {
                if (key == null) {
                    throw new NullPointerException();
                } else {
                    Map<String, ByteString> map = this.internalGetElementImages().getMap();
                    if (!map.containsKey(key)) {
                        throw new IllegalArgumentException();
                    } else {
                        return (ByteString)map.get(key);
                    }
                }
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder clearElementImages() {
                this.internalGetMutableElementImages().getMutableMap().clear();
                return this;
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder removeElementImages(String key) {
                if (key == null) {
                    throw new NullPointerException();
                } else {
                    this.internalGetMutableElementImages().getMutableMap().remove(key);
                    return this;
                }
            }

            /** @deprecated */
            @Deprecated
            public Map<String, ByteString> getMutableElementImages() {
                return this.internalGetMutableElementImages().getMutableMap();
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder putElementImages(String key, ByteString value) {
                if (key == null) {
                    throw new NullPointerException();
                } else if (value == null) {
                    throw new NullPointerException();
                } else {
                    this.internalGetMutableElementImages().getMutableMap().put(key, value);
                    return this;
                }
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder putAllElementImages(Map<String, ByteString> values) {
                this.internalGetMutableElementImages().getMutableMap().putAll(values);
                return this;
            }

            public double getConfidenceThreshold() {
                return this.confidenceThreshold_;
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder setConfidenceThreshold(double value) {
                this.confidenceThreshold_ = value;
                this.onChanged();
                return this;
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder clearConfidenceThreshold() {
                this.confidenceThreshold_ = 0.0D;
                this.onChanged();
                return this;
            }

            public boolean getAllowWeakerMatches() {
                return this.allowWeakerMatches_;
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder setAllowWeakerMatches(boolean value) {
                this.allowWeakerMatches_ = value;
                this.onChanged();
                return this;
            }

            public ClassifierOuterClass.ElementClassificationRequest.Builder clearAllowWeakerMatches() {
                this.allowWeakerMatches_ = false;
                this.onChanged();
                return this;
            }

            public final ClassifierOuterClass.ElementClassificationRequest.Builder setUnknownFields(UnknownFieldSet unknownFields) {
                return (ClassifierOuterClass.ElementClassificationRequest.Builder)super.setUnknownFields(unknownFields);
            }

            public final ClassifierOuterClass.ElementClassificationRequest.Builder mergeUnknownFields(UnknownFieldSet unknownFields) {
                return (ClassifierOuterClass.ElementClassificationRequest.Builder)super.mergeUnknownFields(unknownFields);
            }
        }

        private static final class ElementImagesDefaultEntryHolder {
            static final MapEntry<String, ByteString> defaultEntry;

            private ElementImagesDefaultEntryHolder() {
            }

            static {
                defaultEntry = MapEntry.newDefaultInstance(ClassifierOuterClass.internal_static_ElementClassificationRequest_ElementImagesEntry_descriptor, FieldType.STRING, "", FieldType.BYTES, ByteString.EMPTY);
            }
        }
    }

    public interface ElementClassificationRequestOrBuilder extends MessageOrBuilder {
        String getLabelHint();

        ByteString getLabelHintBytes();

        int getElementImagesCount();

        boolean containsElementImages(String var1);

        /** @deprecated */
        @Deprecated
        Map<String, ByteString> getElementImages();

        Map<String, ByteString> getElementImagesMap();

        ByteString getElementImagesOrDefault(String var1, ByteString var2);

        ByteString getElementImagesOrThrow(String var1);

        double getConfidenceThreshold();

        boolean getAllowWeakerMatches();
    }
}
