//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package ai.test.classifier_client;

import ai.test.classifier_client.ClassifierOuterClass.ClassifiedElements;
import ai.test.classifier_client.ClassifierOuterClass.ElementClassificationRequest;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.protobuf.Descriptors.FileDescriptor;
import io.grpc.*;
import io.grpc.MethodDescriptor.Marshaller;
import io.grpc.MethodDescriptor.MethodType;
import io.grpc.protobuf.ProtoFileDescriptorSupplier;
import io.grpc.protobuf.ProtoMethodDescriptorSupplier;
import io.grpc.protobuf.ProtoServiceDescriptorSupplier;
import io.grpc.protobuf.ProtoUtils;
import io.grpc.stub.AbstractStub;
import io.grpc.stub.ClientCalls;
import io.grpc.stub.ServerCalls;
import io.grpc.stub.ServerCalls.BidiStreamingMethod;
import io.grpc.stub.ServerCalls.ClientStreamingMethod;
import io.grpc.stub.ServerCalls.ServerStreamingMethod;
import io.grpc.stub.ServerCalls.UnaryMethod;
import io.grpc.stub.StreamObserver;
import io.grpc.stub.annotations.RpcMethod;

public final class ClassifierGrpc {
    public static final String SERVICE_NAME = "Classifier";
    private static volatile MethodDescriptor<ElementClassificationRequest, ClassifiedElements> getClassifyElementsMethod;
    private static final int METHODID_CLASSIFY_ELEMENTS = 0;
    private static volatile ServiceDescriptor serviceDescriptor;

    private ClassifierGrpc() {
    }

    @RpcMethod(
            fullMethodName = "Classifier/ClassifyElements",
            requestType = ElementClassificationRequest.class,
            responseType = ClassifiedElements.class,
            methodType = MethodType.UNARY
    )
    public static MethodDescriptor<ElementClassificationRequest, ClassifiedElements> getClassifyElementsMethod() {
        MethodDescriptor getClassifyElementsMethod;
        if ((getClassifyElementsMethod = ClassifierGrpc.getClassifyElementsMethod) == null) {
            Class var1 = ClassifierGrpc.class;
            synchronized(ClassifierGrpc.class) {
                if ((getClassifyElementsMethod = ClassifierGrpc.getClassifyElementsMethod) == null) {
// Hans: in onderstaande aanroep type casts toegevoegd bij setRequestMarshaller en setResponseMarshaller omdat deze bij de build fouten gaven.
                    ClassifierGrpc.getClassifyElementsMethod = getClassifyElementsMethod = MethodDescriptor.newBuilder().setType(MethodType.UNARY)
                            .setFullMethodName(MethodDescriptor.generateFullMethodName("Classifier", "ClassifyElements"))
                            .setSampledToLocalTracing(true)
                            .setRequestMarshaller((MethodDescriptor.Marshaller<Object>) (Marshaller) (ProtoUtils.marshaller( ElementClassificationRequest.getDefaultInstance() )) )
                            .setResponseMarshaller((MethodDescriptor.Marshaller<Object>) (Marshaller) (ProtoUtils.marshaller( ClassifiedElements.getDefaultInstance() )))
                            .setSchemaDescriptor(new ClassifierGrpc.ClassifierMethodDescriptorSupplier("ClassifyElements"))
                            .build();
                }
            }
        }

        return getClassifyElementsMethod;
    }

    public static ClassifierGrpc.ClassifierStub newStub(Channel channel) {
        return new ClassifierGrpc.ClassifierStub(channel);
    }

    public static ClassifierGrpc.ClassifierBlockingStub newBlockingStub(Channel channel) {
        return new ClassifierGrpc.ClassifierBlockingStub(channel);
    }

    public static ClassifierGrpc.ClassifierFutureStub newFutureStub(Channel channel) {
        return new ClassifierGrpc.ClassifierFutureStub(channel);
    }

    public static ServiceDescriptor getServiceDescriptor() {
        ServiceDescriptor result = serviceDescriptor;
        if (result == null) {
            Class var1 = ClassifierGrpc.class;
            synchronized(ClassifierGrpc.class) {
                result = serviceDescriptor;
                if (result == null) {
                    serviceDescriptor = result = ServiceDescriptor.newBuilder("Classifier").setSchemaDescriptor(new ClassifierGrpc.ClassifierFileDescriptorSupplier()).addMethod(getClassifyElementsMethod()).build();
                }
            }
        }

        return result;
    }

    private static final class ClassifierMethodDescriptorSupplier extends ClassifierGrpc.ClassifierBaseDescriptorSupplier implements ProtoMethodDescriptorSupplier {
        private final String methodName;

        ClassifierMethodDescriptorSupplier(String methodName) {
            this.methodName = methodName;
        }

        public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
            return this.getServiceDescriptor().findMethodByName(this.methodName);
        }
    }

    private static final class ClassifierFileDescriptorSupplier extends ClassifierGrpc.ClassifierBaseDescriptorSupplier {
        ClassifierFileDescriptorSupplier() {
        }
    }

    private abstract static class ClassifierBaseDescriptorSupplier implements ProtoFileDescriptorSupplier, ProtoServiceDescriptorSupplier {
        ClassifierBaseDescriptorSupplier() {
        }

        public FileDescriptor getFileDescriptor() {
            return ClassifierOuterClass.getDescriptor();
        }

        public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
            return this.getFileDescriptor().findServiceByName("Classifier");
        }
    }

    private static final class MethodHandlers<Req, Resp> implements UnaryMethod<Req, Resp>, ServerStreamingMethod<Req, Resp>, ClientStreamingMethod<Req, Resp>, BidiStreamingMethod<Req, Resp> {
        private final ClassifierGrpc.ClassifierImplBase serviceImpl;
        private final int methodId;

        MethodHandlers(ClassifierGrpc.ClassifierImplBase serviceImpl, int methodId) {
            this.serviceImpl = serviceImpl;
            this.methodId = methodId;
        }

        public void invoke(Req request, StreamObserver<Resp> responseObserver) {
            switch(this.methodId) {
                case 0:
                    this.serviceImpl.classifyElements((ElementClassificationRequest)request, (StreamObserver<ClassifiedElements>) responseObserver);
                    return;
                default:
                    throw new AssertionError();
            }
        }

        public StreamObserver<Req> invoke(StreamObserver<Resp> responseObserver) {
            switch(this.methodId) {
                default:
                    throw new AssertionError();
            }
        }
    }

    public static final class ClassifierFutureStub extends AbstractStub<ClassifierGrpc.ClassifierFutureStub> {
        private ClassifierFutureStub(Channel channel) {
            super(channel);
        }

        private ClassifierFutureStub(Channel channel, CallOptions callOptions) {
            super(channel, callOptions);
        }

        protected ClassifierGrpc.ClassifierFutureStub build(Channel channel, CallOptions callOptions) {
            return new ClassifierGrpc.ClassifierFutureStub(channel, callOptions);
        }

        public ListenableFuture<ClassifiedElements> classifyElements(ElementClassificationRequest request) {
            return ClientCalls.futureUnaryCall(this.getChannel().newCall(ClassifierGrpc.getClassifyElementsMethod(), this.getCallOptions()), request);
        }
    }

    public static final class ClassifierBlockingStub extends AbstractStub<ClassifierGrpc.ClassifierBlockingStub> {
        private ClassifierBlockingStub(Channel channel) {
            super(channel);
        }

        private ClassifierBlockingStub(Channel channel, CallOptions callOptions) {
            super(channel, callOptions);
        }

        protected ClassifierGrpc.ClassifierBlockingStub build(Channel channel, CallOptions callOptions) {
            return new ClassifierGrpc.ClassifierBlockingStub(channel, callOptions);
        }

        public ClassifiedElements classifyElements(ElementClassificationRequest request) {
            return (ClassifiedElements)ClientCalls.blockingUnaryCall(this.getChannel(), ClassifierGrpc.getClassifyElementsMethod(), this.getCallOptions(), request);
        }
    }

    public static final class ClassifierStub extends AbstractStub<ClassifierGrpc.ClassifierStub> {
        private ClassifierStub(Channel channel) {
            super(channel);
        }

        private ClassifierStub(Channel channel, CallOptions callOptions) {
            super(channel, callOptions);
        }

        protected ClassifierGrpc.ClassifierStub build(Channel channel, CallOptions callOptions) {
            return new ClassifierGrpc.ClassifierStub(channel, callOptions);
        }

        public void classifyElements(ElementClassificationRequest request, StreamObserver<ClassifiedElements> responseObserver) {
            ClientCalls.asyncUnaryCall(this.getChannel().newCall(ClassifierGrpc.getClassifyElementsMethod(), this.getCallOptions()), request, responseObserver);
        }
    }

    public abstract static class ClassifierImplBase implements BindableService {
        public ClassifierImplBase() {
        }

        public void classifyElements(ElementClassificationRequest request, StreamObserver<ClassifiedElements> responseObserver) {
            ServerCalls.asyncUnimplementedUnaryCall(ClassifierGrpc.getClassifyElementsMethod(), responseObserver);
        }

        public final ServerServiceDefinition bindService() {
            return ServerServiceDefinition.builder(ClassifierGrpc.getServiceDescriptor()).addMethod(ClassifierGrpc.getClassifyElementsMethod(), ServerCalls.asyncUnaryCall(new ClassifierGrpc.MethodHandlers(this, 0))).build();
        }
    }
}
