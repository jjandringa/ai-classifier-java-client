/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author handringa
 */
public class FileIO {

    public static void emptyImagesFolder() {
        // Verwijdert eerder opgeslagen image files
        File folder = new File("images");
        for (File file : folder.listFiles()) {
            if (file != null) {
                file.delete();
            }
        }
    }

    public static void storeElementScreenshot(String id, byte[] screenShot) {
        // Image wegschrijven naar een bestand in directory images van project
        String filename = "images/" + id + ".png";
        try {
            File imageFile = new File(filename);
            FileOutputStream file = new FileOutputStream(filename);
            file.write(screenShot);
            file.close();
        } catch (IOException e) {
            System.out.println("Image opslaan is niet gelukt.");
        }
    }

}
