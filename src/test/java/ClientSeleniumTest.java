import ai.test.classifier_client.ClassifierClient;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.interactions.Actions;
import static utils.FileIO.emptyImagesFolder;

public class ClientSeleniumTest {

    public RemoteWebDriver driver;
    public ClassifierClient classifier;

    @Before
    public void setUp() throws MalformedURLException {
        emptyImagesFolder();
        DesiredCapabilities caps = DesiredCapabilities.chrome();
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), caps);
        classifier = new ClassifierClient("127.0.0.1", 50051);
    }

    @After
    public void tearDown() throws InterruptedException {
        if (driver != null) {
            driver.quit();
        }
        classifier.shutdown();
    }

    @Test
    public void testClassifierClient() throws Exception {
        driver.get("https://opensource-demo.orangehrmlive.com/");
        
        List<WebElement> els = classifier.findElementsMatchingLabel(driver, "twitter");
        Assert.assertThat(els, IsCollectionWithSize.hasSize(1));

        for (WebElement el:els) {
            System.out.println("Gevonden webelement: " + el.getLocation().toString() + " Tagname: " + el.getTagName());
        }
     
        els.get(0).click();

        ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));

        Assert.assertEquals(driver.getCurrentUrl(), "https://twitter.com/orangehrm");
    }
    
//    @Test
    public void testClassifierExtra() throws Exception {
        driver.get("https://www.karwei.nl/");
        
        Map<WebElement, byte[]> webElementImages = classifier.getWebElementImages(driver);
        
        Actions actions = new Actions(driver);
        List<WebElement> els;
        
        els = classifier.findElementsMatchingLabel(webElementImages, "cart", 0.12, false);
        Assert.assertThat(els, IsCollectionWithSize.hasSize(1));
        for (WebElement el:els) {
            System.out.println("Gevonden webelement: " + el.getLocation().toString() + " Tagname: " + el.getTagName());
        }
        actions.moveToElement(els.get(0)).click().build().perform();
        Assert.assertEquals(driver.getCurrentUrl(), "https://kassa.karwei.nl/n/cart");
        
        els = classifier.findElementsMatchingLabel(webElementImages, "favorite", 0.2, false);
        Assert.assertThat(els, IsCollectionWithSize.hasSize(1));
        for (WebElement el:els) {
            System.out.println("Gevonden webelement: " + el.getLocation().toString() + " Tagname: " + el.getTagName());
        }
        actions.moveToElement(els.get(0)).click().build().perform();
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.karwei.nl/favorieten");
        
        els = classifier.findElementsMatchingLabel(webElementImages, "facebook", 0.12, false);
        Assert.assertThat(els, IsCollectionWithSize.hasSize(1));
        for (WebElement el:els) {
            System.out.println("Gevonden webelement: " + el.getLocation().toString() + " Tagname: " + el.getTagName());
        }
        actions.moveToElement(els.get(0)).click().build().perform();
        ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.facebook.com/karwei");
        
    }


}