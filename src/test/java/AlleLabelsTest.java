
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.openqa.selenium.WebElement;

public class AlleLabelsTest extends ClientSeleniumTest {
    
    private final ArrayList<String> labels;

    public AlleLabelsTest() {
        this.labels = new ArrayList<>(
                Arrays.asList(
                        "unclassified",
                        "add",
                        "airplane",
                        "alarm",
                        "arrow down",
                        "arrow left",
                        "arrow right",
                        "arrow up",
                        "attach",
                        "bag",
                        "barcode",
                        "battery",
                        "bluetooth",
                        "bookmark",
                        "brightness",
                        "calculator",
                        "calendar",
                        "call",
                        "camera",
                        "car",
                        "cart",
                        "chart",
                        "check mark",
                        "clock",
                        "close",
                        "cloud",
                        "computer",
                        "contrast",
                        "credit card",
                        "crop",
                        "cursor",
                        "cut",
                        "dashboard",
                        "delete",
                        "dollar",
                        "download",
                        "edit",
                        "external link",
                        "eye",
                        "fab",
                        "facebook",
                        "fast forward",
                        "favorite",
                        "file",
                        "filter",
                        "fingerprint",
                        "fire",
                        "flag",
                        "flashlight",
                        "folder",
                        "gift",
                        "globe",
                        "gmail",
                        "google",
                        "grid",
                        "headphones",
                        "home",
                        "inbox",
                        "info",
                        "laptop",
                        "light bulb",
                        "link",
                        "location",
                        "lock",
                        "mail",
                        "map",
                        "maximize",
                        "megaphone",
                        "menu",
                        "microphone",
                        "minimize",
                        "mobile",
                        "moon",
                        "music",
                        "mute",
                        "notifications",
                        "overflow menu",
                        "pinterest",
                        "play",
                        "printer",
                        "profile avatar",
                        "qr code",
                        "question",
                        "refresh",
                        "reply",
                        "rewind",
                        "save",
                        "search",
                        "send",
                        "settings",
                        "share",
                        "signal",
                        "sort",
                        "tag",
                        "television",
                        "thumbs up",
                        "ticket",
                        "trash",
                        "trophy",
                        "twitter",
                        "unlock",
                        "upload",
                        "user",
                        "video camera",
                        "volume",
                        "warning"));
    }

    @Test
    public void testAlleLabels() throws Exception {
        // Scant web elementen van een internet pagina en maakt daar waar mogelijk images van
        // Bepaalt vervolgens de waarschijnlijkheid dat gevonden images overeenkomen met bekende images
        
        driver.get("https://www.qquest.nl/");
        Map<WebElement, byte[]> webElementImages = classifier.getWebElementImages(driver);
        
        List<WebElement> els;
        for (String label : labels) {
            els = classifier.findElementsMatchingLabel(webElementImages, label, 0.2, true);
            
            for (WebElement el:els) {
                System.out.println(label + ": gevonden webelement: " + el.getLocation().toString() + " tagname: " + el.getTagName());
            }
        }  
    }

}
