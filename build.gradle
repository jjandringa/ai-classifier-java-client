plugins {
    id 'java'
    id 'com.google.protobuf' version '0.8.8'
    id 'application'
    id 'idea'
    id 'maven'
}

group 'ai.test'
version '1.0-SNAPSHOT'

sourceCompatibility = 1.8

repositories {
    maven { // The google mirror is less flaky than mavenCentral()
        url "https://maven-central.storage-download.googleapis.com/repos/central/data/" }
    mavenCentral()
}

def grpcVersion = '1.25.0' // CURRENT_GRPC_VERSION
def protobufVersion = '3.10.1'
def protocVersion = protobufVersion

dependencies {
    implementation "io.grpc:grpc-protobuf:${grpcVersion}"
    implementation "io.grpc:grpc-stub:${grpcVersion}"
    implementation "io.grpc:grpc-okhttp:${grpcVersion}"
    implementation group: 'org.seleniumhq.selenium', name: 'selenium-java', version: '3.141.59'

    compileOnly "javax.annotation:javax.annotation-api:1.2"
    compile 'org.apache.commons:commons-io:1.3.2'

    testCompile group: 'com.google.guava', name: 'guava', version: '28.1-jre'
    testCompile group: 'junit', name: 'junit', version: '4.12'
    testCompile group: 'org.hamcrest', name: 'hamcrest-junit', version: '2.0.0.0'
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:${protocVersion}"
    }
    plugins {
        grpc {
            artifact = "io.grpc:protoc-gen-grpc-java:${grpcVersion}"
        }
    }
    generateProtoTasks {
        all()*.plugins {
            grpc {}
        }
    }
}

// Inform IDEs like IntelliJ IDEA, Eclipse or NetBeans about the generated code.
sourceSets {
    main {
        java {
            srcDirs 'build/generated/source/proto/main/grpc'
            srcDirs 'build/generated/source/proto/main/java'
        }
    }
}

startScripts.enabled = false

task classifierClient(type: CreateStartScripts) {
    mainClassName = 'ai.test.classifier_client'
    applicationName = 'test-ai-classifier-client'
    outputDir = new File(project.buildDir, 'tmp')
    classpath = startScripts.classpath
}

applicationDistribution.into('bin') {
    from(classifierClient)
    fileMode = 0755
}

task fatJar(type: Jar) {
    manifest {
        attributes 'Main-Class': 'ai.test.classifier_client.ClassifierClient'
    }
    baseName = project.name + '-all'
    from { 
        configurations.runtimeClasspath.collect { it.isDirectory() ? it : zipTree(it) }
    }
    with jar
}

test {
    outputs.upToDateWhen {false}
    useJUnit()
    testLogging {
        exceptionFormat = 'full'
        showStandardStreams = true
    }
}
